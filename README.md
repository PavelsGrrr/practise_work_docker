# Darbs ar Docker katacoda.com
## Ievads Docker images
- [ ] Seko instrukcijām https://www.katacoda.com/courses/docker/2 un izveido savu Docker image - pamēģini izveidot Docker image, kam nosaukumā ir Tavs vārds
- [ ] Kad tas ir izdevies, pievieno komentārā uzdevumam, ko atgriež sekojošās komandas:
```sh
docker images
docker ps
curl -i http://docker
```
## Ievads Docker containers
- [ ] Seko instrukcijām https://www.katacoda.com/courses/docker/create-nginx-static-web-server un izveido statisku web lapu, izmantojot Docker konteineri
# Darbs ar Docker savā virtuālajā serverī
## Darba vides sagatavošana izmantojot Cloudformation un pieslēgšanās instancei
- [ ] Lejuplādē `docker-stack.yaml`
- [ ] Atver AWS konsoli https://952122846739.signin.aws.amazon.com/console un ieej savā kontā:
- [ ] Pārliecinies vai augšējā labā stūrī reģions izvēlēts *Ireland* (Europe (Ireland) eu-west-1). Ja nē, nomaini to uz *Ireland*
- [ ] Atver Cloudformation konsoli meklējot resursu *CloudFormation* un izvēloties *Create Stack* vai atverot https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/create/template, kas atver formu pirmā CF stack izveidei.
  1. *Specify template* solis - pie *Prepare template* izvēlies *Template is ready*, pie *Specify template* - *Upload a template file* -> Choose file un augšuplādē `docker-stack.yaml` failu -> Next
  1. *Specify stack details*
      - Stack name - tavsVards-docker-stack
      - InstanceTypeParameter - atstāj noklusējuma vērtību *t2.micro*
      - OwnerName - tavsVards bez atstarpēm, mīkstinājuma un garumzīmēm
    Spied *Next*
  1. *Configure stack options*
      - Pie Tags pievienojam jaunu tag
      - Key: School
      - Value: RTU2022
      - Pašā apkašā spiežam "Next"
  1. *Review* - pārskati stack detaļas un spied *Create stack*
- [ ] Lai apskatītu progresu, atjauno lapu izmantojot pārlūka vai AWS atjaunošanas pogu labējā augšējā stūrī. Kad statuss ir *CREATE_COMPLETE*, esi izveidojis CloudFormation stack. Spied uz Resources, tad uz *DockerInstance* Physical ID, kas jaunā lapā atvērs izveidoto EC2 instanci. Pieslēdzies instancei ar SSM, izvēloties instanci un spiežot "Connect" labējā augšējā stūrī. Tad "Session Manager" tab spied "Connect"

## _Petclinic_ aplikācijas uzstādīšana, izmantojot Docker
- [ ] Pārvietojies uz home directory un pārliecinies vai docker ir veiksmīgi ieinstalēts. Tad izveido Dockerfile (vari izmantot vi vai nano atkarībā ar ko ērtāk strādāt)
  ```sh
  cd ~
  sudo docker version
  vi Dockerfile # ja ērtāk strādāt ar vi
  nano Dockerfile # ja ērtāk strādāt ar nano
  ```
- [ ] Iekopē zemāk esošo kodu Dockerfile un saglabā to, izej no vi/nano
  ```Dockerfile
  FROM openjdk:18-jdk-alpine3.14
  ENV PETCLINIC_VERSION=5.3.0
  RUN apk update && \
      apk add tar \
              wget
  RUN wget -O petclinic.tar.gz "https://github.com/spring-petclinic/spring-framework-petclinic/archive/refs/tags/v${PETCLINIC_VERSION}.tar.gz" -q \
      && tar -xzf petclinic.tar.gz \
      && rm -f petclinic.tar.gz
  RUN apk del tar \
              wget

  EXPOSE 8080
  WORKDIR spring-framework-petclinic-${PETCLINIC_VERSION}
  ENTRYPOINT ["./mvnw", "jetty:run-war"]
  ```
- [ ] Izmanto zemāk esošo komandu, lai uzbūvētu Docker image - petclinic ar tag 5.3.0; pēc tam apskati, ka image ir uzbūvēts un apskati tā detaļas. Tad novelc no Docker registry publisko Petclinic Docker image un apskati abu image detaļas.0
🎉 Ieraksti uzdevuma komentārā, kāpēc, Tavuprāt, mūsu izveidotais Docker image ir lielāks:
  ```sh
  sudo docker build -t petclinic:5.3.0 .
  sudo docker images
  sudo docker pull springcommunity/spring-framework-petclinic
  sudo docker images
  ```
- [ ] Izmantojot tikko uzbūvēto image, palaid konteineri ar Petclinic aplikāciju. Seko aplikācijas uzstādīšanas progresam ar `docker log` komandu:
  ```sh
  sudo docker run -d --restart always --name petclinic -p 80:8080 petclinic:5.3.0
  sudo docker logs -n 50 -f petclinic
  ```
- [ ] Kad vairs nav izvada docker log un Jetty ziņo, ka serveris ir startēts, piekļūsti aplikācijai izmantojot instances publisko IP adresi (Publisko IP adresi atradīsi meklējot savu instanci https://eu-west-1.console.aws.amazon.com/ec2/home?region=eu-west-1#Instances: *Details* tab zem *Public IPv4 address* vai izvēloties savu stack https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false *Outputs* tab pie *ApplicationUrl*).
🎉 Ievieto ekrānuzņēmumu ar Petclinic aplikāciju pie uzdevuma komentārā

## Papildus uzdevums, ja vēlies nākamajā laboratorijas darbā izmantot sevis sagatavoto attēlu
- [ ] Izveido vai ieej savā kontā https://hub.docker.com/
- [ ] Izveido jaunu repozitoriju petclinic https://hub.docker.com/ spiežot uz "Create Repository" pogas
    - Name - petclinic
    - Description atstāj tukšu
    - Visibility - Public
    Spied *Create*
- [ ] Savā EC2 instancē pārsauc petclinic:5.3.0 Docker image uz tavsDockerHubLietotajvards/petclinic:5.3.0, ielogojies un nosūti Docker image uz publisko DockerHub
  ```sh
  sudo docker tag petclinic:5.3.0 tavsDockerHubLietotajvards/petclinic:5.3.0
  sudo docker login -u tavsDockerHubLietotajvards
  sudo docker push tavsDockerHubLietotajvards/petclinic:5.3.0
  ```
- [ ] Atsvaidzini repozitoriju https://hub.docker.com/

## Resursu tīrīšana
- [ ] Atver Cloudformation konsoli meklējot resursu *CloudFormation* vai atverot https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false un atrodi savu stack pēc sava vārda, izvēlies to un spied *Delete* -> *Delete stack*
Lai apskatītu progresu, atjauno lapu izmantojot pārlūka vai AWS atjaunošanas pogu labējā augšējā stūrī. https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=deleted&filteringText=&viewNested=true&hideStacks=false&stackId= meklējot pēc sava vārda vari pārliecināties, ka statuss norāda *DELETE_COMPLETE*

Citus ar Docker saistītus uzdevumus vari atrast https://www.katacoda.com/courses/docker
